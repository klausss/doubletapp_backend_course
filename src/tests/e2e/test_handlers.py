import pytest
from app.internal.transport.bot.handlers import start, set_phone, check_balance
from app.internal.transport.bot.replies import StartReply, SetPhoneReply, BalanceReply
from app.internal.models.tguser import TgUser


@pytest.mark.e2e
@pytest.mark.django_db
async def test_start(update1, context1):
    await start(update1, context1)

    update1.message.reply_text.assert_called_once_with(StartReply.USER_SAVED)

    user_from_db = await TgUser.objects.aget(id=55)
    assert user_from_db.username == 'user55'


@pytest.mark.e2e
@pytest.mark.django_db
async def test_start_existing_user(update1, context1):
    await start(update1, context1)
    update1.message.reply_text.assert_called_once_with(StartReply.USER_EXISTS)


@pytest.mark.e2e
@pytest.mark.django_db
async def test_set_phone_existing_user_valid(update1, context1_valid_phone):
    await set_phone(update1, context1_valid_phone)

    user_from_db = await TgUser.objects.aget(id=55)
    assert user_from_db.phone_number == '+79001231212'

    update1.message.reply_text.assert_called_once_with(SetPhoneReply.PHONE_SET)


@pytest.mark.e2e
@pytest.mark.django_db
async def test_set_phone_existing_user_invalid(update1, context1_invalid_phone):
    await set_phone(update1, context1_invalid_phone)

    user_from_db = await TgUser.objects.aget(id=55)
    assert user_from_db.phone_number == '+79001231212'

    update1.message.reply_text.assert_called_once_with(SetPhoneReply.INCORRECT_SET_PHONE)


@pytest.mark.e2e
@pytest.mark.django_db
async def test_set_phone_nonexisting_user(update2, context2):
    await set_phone(update2, context2)
    update2.message.reply_text.assert_called_once_with(SetPhoneReply.USER_DOESNT_EXIST)


@pytest.mark.e2e
@pytest.mark.django_db
async def test_check_balance_no_cards(update1, context1):
    await check_balance(update1, context1)
    update1.message.reply_text.assert_called_once_with(BalanceReply.SMTH_WRONG)
