import pytest
from unittest.mock import MagicMock, AsyncMock


@pytest.fixture
def user1():
    usr = MagicMock()
    usr.id = 55
    usr.username = 'user55'
    usr.first_name = 'User55'
    usr.last_name = 'Sample55'
    return usr


@pytest.fixture
def update1(user1):
    upd = MagicMock()
    upd.effective_chat = user1
    upd.effective_user = user1
    upd.message.reply_text = AsyncMock(return_value=None)
    return upd


@pytest.fixture
def context1():
    ctx = MagicMock()
    ctx.args = []
    return ctx


@pytest.fixture
def context1_valid_phone():
    ctx = MagicMock()
    ctx.args = ['+79001231212']
    return ctx


@pytest.fixture
def context1_invalid_phone():
    ctx = MagicMock()
    ctx.args = ['15001231212']
    return ctx


@pytest.fixture
def user2():
    usr = MagicMock()
    usr.id = 66
    usr.username = 'user66'
    usr.first_name = 'User66'
    usr.last_name = 'Sample66'
    return usr


@pytest.fixture
def update2(user2):
    upd = MagicMock()
    upd.effective_chat = user2
    upd.effective_user = user2
    upd.message.reply_text = AsyncMock(return_value=None)
    return upd


@pytest.fixture
def context2():
    ctx = MagicMock()
    ctx.args = []
    return ctx
