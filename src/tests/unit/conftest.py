import pytest
from app.internal.models.tguser import TgUser
from app.internal.models.card import Card
from app.internal.models.account import Account


@pytest.fixture
def tg_user():
    user = TgUser(
        id=7,
        username="jamesbond",
        first_name="James",
        last_name="Bond"
    )
    return user


@pytest.fixture
def account_number():
    return "20202020202020202020"


@pytest.fixture
def account(account_number, tg_user):
    acc = Account(
        account_number=account_number,
        owner=tg_user,
        money=1000
    )
    return acc


@pytest.fixture
def card(tg_user, account):
    card = Card(
        card_number='5586751192007929',
        expiry_date='01/30',
        cvv_code='190',
        account_number=account,
        card_owner=tg_user
    )
    return card
