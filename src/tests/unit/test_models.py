import pytest
import datetime
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from app.internal.services.user_service import save_user, get_user_by_id_async, get_user_by_username, \
    set_user_phone_number, user_exists
from app.internal.models.tguser import TgUser
from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.services.custom_exceptions import UserNotFoundException


@pytest.mark.unit
@pytest.mark.django_db
async def test_user_create(tg_user):
    await save_user(tg_user)

    user_from_db = await TgUser.objects.aget(id=tg_user.id)
    assert user_from_db.username == "jamesbond"
    assert user_from_db.first_name == "James"
    assert user_from_db.last_name == "Bond"


@pytest.mark.unit
@pytest.mark.django_db
async def test_get_existing_user_by_id():
    user = await get_user_by_id_async(7)
    assert user.username == "jamesbond"


@pytest.mark.unit
@pytest.mark.django_db
async def test_get_nonexisting_user_by_id():
    with pytest.raises(UserNotFoundException):
        user = await get_user_by_id_async(111)
        assert user is None


@pytest.mark.unit
@pytest.mark.django_db
async def test_get_existing_user_by_username():
    user = await get_user_by_username("jamesbond")

    assert user.username == "jamesbond"
    assert user.first_name == "James"
    assert user.last_name == "Bond"
    assert user.id == 7


@pytest.mark.unit
@pytest.mark.django_db
async def test_get_nonexisting_user_by_username():
    with pytest.raises(UserNotFoundException):
        user = await get_user_by_username("somerandomusername")
        assert user is None


@pytest.mark.unit
@pytest.mark.django_db
async def test_set_valid_phone():
    phone_num = '+79001231231'
    await set_user_phone_number(7, phone_num)

    user_from_db = await TgUser.objects.aget(id=7)
    assert user_from_db.phone_number == phone_num


@pytest.mark.unit
@pytest.mark.django_db
async def test_set_not_valid_phone():
    phone_num = '15001231212'

    with pytest.raises(ValidationError):
        await set_user_phone_number(7, phone_num)


@pytest.mark.unit
@pytest.mark.django_db
async def test_valid_user_exists():
    exists = await user_exists(7)
    assert exists


@pytest.mark.unit
@pytest.mark.django_db
async def test_not_valid_user_doesnt_exist():
    exists = await user_exists(123)
    assert not exists


@pytest.mark.unit
@pytest.mark.django_db
def test_create_valid_account(account, account_number):
    account.save()

    account_from_db = Account.objects.get(account_number=account_number)
    user_from_db = TgUser.objects.get(id=7)

    assert account_from_db.account_number == account_number
    assert account_from_db.owner == user_from_db
    assert account_from_db.money == 1000


@pytest.mark.unit
@pytest.mark.django_db
def test_create_invalid_account():
    with pytest.raises(IntegrityError):
        Account.objects.create(account_number=123, money=1)


@pytest.mark.unit
@pytest.mark.django_db
def test_create_valid_card(card, account_number, account):
    account.save()
    card.save()

    user_from_db = TgUser.objects.get(id=7)
    account_from_db = Account.objects.get(account_number=account_number)
    card_from_db = Card.objects.get(card_number=card.card_number)
    assert card_from_db.expiry_date == datetime.date(2030, 1, 31)
    assert card_from_db.cvv_code == "190"
    assert card_from_db.account_number == account_from_db
    assert card_from_db.card_owner == user_from_db
