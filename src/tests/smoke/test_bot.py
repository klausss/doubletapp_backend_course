import pytest
from telegram.ext import Application
from config.settings import BOT_TOKEN


@pytest.mark.smoke
async def test_bot():
    bot = Application.builder().token(BOT_TOKEN).build()
    webhook_info = await bot.updater.bot.get_webhook_info()
    assert webhook_info
