import pytest
import requests


@pytest.mark.smoke
def test_api_found():
    url = "https://ianikina.backend23.2tapp.cc/api/me"
    response = requests.get(url, params={"id": 1})
    assert response.status_code == 200
    assert response.json()["username"] == "user1"


def test_api_not_found():
    url = "https://ianikina.backend23.2tapp.cc/api/me"
    response = requests.get(url, params={"id": 321})
    assert response.status_code == 404
    assert response.json() == "User doesn't exist or the phone number isn't filled."
