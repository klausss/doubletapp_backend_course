import pytest
from app.internal.models.tguser import TgUser
from app.internal.models.account import Account
from app.internal.models.card import Card


@pytest.fixture(scope="module")
def tg_users():
    users = []
    for i in range(3):
        user = TgUser(
            id=(i + 1),
            username=f"user{i + 1}",
            first_name=f"User{i + 1}",
            last_name=f"Sample{i + 1}"
        )
        users.append(user)
    return users


@pytest.fixture(scope="module")
def accounts(tg_users):
    accs_range = [2, 1]
    accs_out = []
    for i in range(2):
        accs_out.append([])
        for j in range(accs_range[i]):
            money = 900 + (i + 1) * 10
            account = Account(
                account_number=f'{i + 1}{i + 1}{j + 1}',
                owner=tg_users[i],
                money=money
            )
            accs_out[i].append(account)
    return accs_out


@pytest.fixture(scope="module")
def card_numbers():
    return [['5258284815511629', '5476165593274926'], ['4545294534692119']]


@pytest.fixture(scope="module")
def cards(tg_users, accounts, card_numbers):
    user_cards = []
    for i in range(2):
        for j in range(len(card_numbers[i])):
            card = Card(
                card_number=card_numbers[i][j],
                expiry_date='01/30',
                cvv_code='123',
                account_number=accounts[i][j],
                card_owner=tg_users[i]
            )
            user_cards.append(card)
    return user_cards


# user1, 111, 5258284815511629, 910; user1, 112, 5476165593274926, 910; user2, 221, 4545294534692119, 920
@pytest.fixture(scope="module")
def django_db_setup(django_db_setup, django_db_blocker, tg_users, accounts, cards):
    with django_db_blocker.unblock():
        for i in range(3):
            tg_users[i].save()
        for i in range(2):
            for j in range(len(accounts[i])):
                accounts[i][j].save()
        for i in range(3):
            cards[i].save()
