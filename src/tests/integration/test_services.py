import pytest
import decimal
from app.internal.services.user_service import add_user_to_favorites, remove_user_from_favorites, list_favorite_users
from app.internal.services.banking_service import get_user_balance, get_user_cards, check_account_exists, \
    get_account_number_by_username, get_account_number_by_card, transfer_money
from app.internal.services.custom_exceptions import CantSendMoneyException, UserAlreadyExistsException, \
    UserNotFoundException, CardNotFoundException
from app.internal.models.tguser import TgUser
from app.internal.models.account import Account


@pytest.mark.integration
@pytest.mark.django_db
async def test_add_valid_user_to_favorites():
    await add_user_to_favorites(1, 'user2')

    user_from_db = await TgUser.objects.aget(id=1)
    fav_user = await user_from_db.favorite_users.afirst()

    assert fav_user.username == "user2"
    assert fav_user.first_name == "User2"
    assert fav_user.last_name == "Sample2"


@pytest.mark.integration
@pytest.mark.django_db
async def test_add_invalid_user_to_favorites():
    with pytest.raises(CantSendMoneyException):
        await add_user_to_favorites(1, 'user3')


@pytest.mark.integration
@pytest.mark.django_db
async def test_add_existing_user_to_favorites():
    with pytest.raises(UserAlreadyExistsException):
        await add_user_to_favorites(1, 'user2')


@pytest.mark.integration
@pytest.mark.django_db
async def test_add_nonexisting_user_to_favorites():
    with pytest.raises(UserNotFoundException):
        await add_user_to_favorites(1, 'user321')


@pytest.mark.integration
@pytest.mark.django_db
async def test_add_user_to_his_favorites():
    with pytest.raises(CantSendMoneyException):
        await add_user_to_favorites(1, 'user1')


@pytest.mark.integration
@pytest.mark.django_db
async def test_remove_valid_user_from_favorites():
    await remove_user_from_favorites(1, 'user2')

    user_from_db = await TgUser.objects.aget(id=1)
    removed_user_exists = await user_from_db.favorite_users.filter(username='user2').aexists()

    assert not removed_user_exists


@pytest.mark.integration
@pytest.mark.django_db
async def test_remove_deleted_user_from_favorites():
    with pytest.raises(UserNotFoundException):
        await remove_user_from_favorites(1, 'user2')


@pytest.mark.integration
@pytest.mark.django_db
async def test_list_favorite_users():
    await add_user_to_favorites(1, 'user2')

    fav_users_queryset = await list_favorite_users(1)
    fav_users_list = [user async for user in fav_users_queryset]

    assert len(fav_users_list) == 1
    assert fav_users_list[0].username == 'user2'


@pytest.mark.integration
@pytest.mark.django_db
async def test_get_valid_balance_one_card():
    balance = await get_user_balance(2)

    assert balance[0][0] == '*2119'
    assert balance[0][1] == decimal.Decimal(920)


@pytest.mark.integration
@pytest.mark.django_db
async def test_get_balance_for_user_without_cards():
    with pytest.raises(CardNotFoundException):
        await get_user_balance(3)


@pytest.mark.integration
@pytest.mark.django_db
async def test_get_balance_two_cards():
    balance = await get_user_balance(1)
    balance.sort()

    assert len(balance) == 2
    assert balance[0][0] == '*1629'
    assert balance[0][1] == decimal.Decimal(910)
    assert balance[1][0] == '*4926'
    assert balance[1][1] == decimal.Decimal(910)


@pytest.mark.integration
@pytest.mark.django_db
async def test_get_user_cards():
    cards = await get_user_cards(1)
    card_numbers = [c.card_number for c in cards]
    card_numbers.sort()

    assert len(cards) == 2
    assert card_numbers[0] == '5258284815511629'
    assert card_numbers[1] == '5476165593274926'


@pytest.mark.integration
@pytest.mark.django_db
async def test_check_valid_account_exists():
    exists = await check_account_exists('111')
    assert exists


@pytest.mark.integration
@pytest.mark.django_db
async def test_check_invalid_account_exists():
    exists = await check_account_exists('8800')
    assert not exists


@pytest.mark.integration
@pytest.mark.django_db
async def test_get_account_number_by_username_valid():
    account_number = await get_account_number_by_username('user2')
    assert account_number == '221'


@pytest.mark.integration
@pytest.mark.django_db
async def test_get_account_number_by_username_invalid():
    with pytest.raises(CantSendMoneyException):
        await get_account_number_by_username('user3')


@pytest.mark.integration
@pytest.mark.django_db
async def test_get_account_number_by_card_valid():
    card_number = '5258284815511629'
    account_number = await get_account_number_by_card(card_number)
    assert account_number == '111'


@pytest.mark.integration
@pytest.mark.django_db
async def test_get_account_number_by_card_invalid():
    card_number = '3434343434343434'
    with pytest.raises(CardNotFoundException):
        await get_account_number_by_card(card_number)


@pytest.mark.integration
@pytest.mark.django_db
async def test_transfer_money_valid():
    acc_from_num = '111'
    acc_to_num = '221'
    amount = decimal.Decimal(10)
    await transfer_money(acc_from_num, acc_to_num, amount)

    acc_from = await Account.objects.aget(account_number='111')
    acc_to = await Account.objects.aget(account_number='221')

    assert acc_from.money == decimal.Decimal(900)
    assert acc_to.money == decimal.Decimal(930)
