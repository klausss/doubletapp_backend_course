from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from app.internal.transport.rest.serializers import TgUserSerializer
from app.internal.services.user_service import get_user_by_id


class TgUserInfo(APIView):
    def get(self, request):
        user_id = request.GET.get("id")
        user = get_user_by_id(user_id)
        if not user:
            return Response("User doesn't exist or the phone number isn't filled.", status=status.HTTP_404_NOT_FOUND)
        user_serialized = TgUserSerializer(user)
        return Response(data=user_serialized.data, status=status.HTTP_200_OK)
