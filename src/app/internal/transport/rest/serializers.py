from rest_framework import serializers

from app.internal.models.tguser import TgUser


class TgUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = TgUser
        fields = ("id", "username", "phone_number", "first_name", "last_name")
