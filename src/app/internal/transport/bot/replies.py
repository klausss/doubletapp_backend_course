class StartReply:
    USER_EXISTS = "А я тебя уже знаю!"
    USER_SAVED = "Привет! Я тебя записал))"


class SetPhoneReply:
    USER_DOESNT_EXIST = "Я тебя не знаю :(. Напиши /start, а затем /set_phone +79001234567" " со своим номером телефона."
    INCORRECT_SET_PHONE = "Введи номер телефона после команды. Например, так:\n/set_phone +79001234567"
    PHONE_SET = "Номер телефона успешно сохранен! Ура!)"


class MeReply:
    USER_INFO = "Вот что я о тебе знаю:\n{user_info}" + "\nА еще ты хороший человек!"
    PHONE_ISNT_SET = "Мне нужен твой номер телефона. Напиши /set_phone +79001234567 с твоим номером телефона."


class BalanceReply:
    BALANCE = "Баланс твоих карт:\n"
    SMTH_WRONG = "У тебя нет карт или я тебя не знаю :("


class FavoritesReply:
    INCORRECT_ADD = "Неправильный ввод. Вводи: /add @username"
    ADD_SUCCESS = "Пользователь @{username} успешно добавлен в избранное"
    USER_NOT_FOUND = "Пользователь не найден"
    USER_EXISTS_IN_FAV = "Этот пользователь уже в избранном"
    CANT_SEND_MONEY = "Этому пользователю нельзя отправлять деньги"

    INCORRECT_REMOVE = "Неправильный ввод. Вводи: /remove @username"
    REMOVE_SUCCESS = "Пользователь @{username} успешно удален из избранного"

    FAVORITES = " - список ваших избранных пользователей"
    FAVORITES_EMPTY = "Список избранных пользователей пуст"


class SendMoneyReply:
    USER_NOT_FOUND = "Пользователь не найден"
    NO_CARDS = "Ошибка: нет карт"
    START_SEND = "Начинаем перевод. Для отмены пиши /cancel.\nВыбери карту, с которой необходимо выполнить перевод"
    SEND_BY_FAVORITE = "Избранное"
    SEND_BY_USERNAME = "По юзернейму"
    SEND_BY_CARD = "По номеру карты"
    SEND_BY_ACCOUNT = "По номеру аккаунта"
    ENTER_USERNAME = "Введи юзернейм в формате @username"
    ENTER_CARD = "Введи номер карты"
    ENTER_ACCOUNT = "Введи номер аккаунта"
    CHOOSE_FAVORITE_USER = "Выбери пользователя"
    FAVORITES_EMPTY = "Список избранных пользователей пуст"
    CARD_NOT_FOUND = "Карта не найдена"
    CHOOSE_METHOD = "Выбери способ перевода"
    WRONG_METHOD = "Неверный способ перевода"
    CANT_SEND_MONEY = "Пользователю нельзя отправить деньги"
    ACCOUNT_NOT_FOUND = "Аккаунт не найден"
    ENTER_AMOUNT = "Введи сумму перевода"
    INCORRECT_AMOUNT = "Некорректный ввод"
    SEND_ERROR = "Ошибка перевода"
    INSUFFICIENT_FUNDS = "Недостаточно средств"
    SEND_SUCCESS = "Перевод прошел успешно!"
    CANCEL = "Отмена"


class CommonReply:
    HELP = """
    Привет! Я могу:
    /start - начать наше знакомство и записать себе информацию о тебе,
    /set_phone +79001234567 - записать твой номер телефона (не забудь указать его после команды)
    p.s. его можно обновлять))
    /me - отправить тебе все, что я записал (только сначала надо записать! так что не забудь выполнить /start и /set_phone)
    /help - вывести этот текст :)
    /balance - проверить баланс
    /add @username - добавить пользователя в избранное
    /remove @username - удалить пользователя из избранного
    /list - вывести список избранных пользователей
    /send - перевести деньги
            """

    UNKNOWN = "Моя твоя не понимать... Почитай /help, пжл!"
