import decimal
from decimal import Decimal
from django.db.utils import IntegrityError
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import ContextTypes, ConversationHandler, CommandHandler, filters, MessageHandler
from app.internal.services.user_service import get_user_by_id_async, list_favorite_users
from app.internal.services.banking_service import get_user_cards, get_account_number_by_card, \
    get_account_number_by_username, check_account_exists, transfer_money
from app.internal.services.custom_exceptions import UserNotFoundException, CantSendMoneyException, \
    CardNotFoundException, AccountNotFoundException, FavoritesEmptyException
from app.internal.transport.bot.replies import SendMoneyReply

CHOOSE_SEND, PROCESS_SEND, SEND_BY_USERNAME, SEND_BY_ACCOUNT, SEND_BY_CARD, FINISH_SEND = range(6)

SEND_METHODS = {
    SendMoneyReply.SEND_BY_FAVORITE: (SendMoneyReply.CHOOSE_FAVORITE_USER, SEND_BY_USERNAME),
    SendMoneyReply.SEND_BY_USERNAME: (SendMoneyReply.ENTER_USERNAME, SEND_BY_USERNAME),
    SendMoneyReply.SEND_BY_CARD: (SendMoneyReply.ENTER_CARD, SEND_BY_CARD),
    SendMoneyReply.SEND_BY_ACCOUNT: (SendMoneyReply.ENTER_ACCOUNT, SEND_BY_ACCOUNT)
}


async def start_send(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    card_numbers = []
    try:
        user = await get_user_by_id_async(update.effective_user.id)
        cards = await get_user_cards(user.id)
        card_numbers = [c.card_number for c in cards]
    except UserNotFoundException:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.USER_NOT_FOUND)
        return ConversationHandler.END

    if not card_numbers:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.NO_CARDS)
        return ConversationHandler.END

    keyboard = [card_numbers]
    reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)
    await update.message.reply_text(
        SendMoneyReply.START_SEND,
        reply_markup=reply_markup)
    return CHOOSE_SEND


async def choose_send_method(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    card_number = update.message.text
    try:
        account_number = await get_account_number_by_card(card_number)
        context.user_data["acc_from"] = account_number
    except CardNotFoundException:
        await update.message.reply_text(SendMoneyReply.CARD_NOT_FOUND, reply_markup=ReplyKeyboardRemove())
        return ConversationHandler.END
    keyboard = [list(SEND_METHODS.keys())]
    reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)
    await update.message.reply_text(SendMoneyReply.CHOOSE_METHOD, reply_markup=reply_markup)
    return PROCESS_SEND


async def process_send_method(update: Update, _) -> int:
    send_method = update.message.text

    if send_method == SendMoneyReply.SEND_BY_FAVORITE:
        try:
            favorite_users = await list_favorite_users(update.effective_user.id)
            keyboard = [["@" + user.username async for user in favorite_users]]
            reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)
        except UserNotFoundException:
            await update.message.reply_text(SendMoneyReply.USER_NOT_FOUND, reply_markup=ReplyKeyboardRemove())
            return ConversationHandler.END
        except FavoritesEmptyException:
            await update.message.reply_text(SendMoneyReply.FAVORITES_EMPTY, reply_markup=ReplyKeyboardRemove())
            return ConversationHandler.END
    else:
        reply_markup = ReplyKeyboardRemove()

    try:
        await update.message.reply_text(SEND_METHODS[send_method][0], reply_markup=reply_markup)
        return SEND_METHODS[send_method][1]
    except KeyError:
        await update.message.reply_text(SendMoneyReply.WRONG_METHOD, reply_markup=reply_markup)
        return ConversationHandler.END


async def process_send_by_username(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    username = update.message.text[1:]
    try:
        account_number = await get_account_number_by_username(username)
        context.user_data["acc_to"] = account_number
        return await enter_amount(update, context)
    except UserNotFoundException:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.USER_NOT_FOUND)
        return ConversationHandler.END
    except CantSendMoneyException:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.CANT_SEND_MONEY)
        return ConversationHandler.END


async def process_send_by_account(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    account_number = update.message.text
    if not check_account_exists(account_number):
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.ACCOUNT_NOT_FOUND)
        return ConversationHandler.END
    context.user_data["acc_to"] = account_number
    return await enter_amount(update, context)


async def process_send_by_card(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    card_number = update.message.text
    try:
        account_number = await get_account_number_by_card(card_number)
        context.user_data["acc_to"] = account_number
        return await enter_amount(update, context)
    except CardNotFoundException:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.CARD_NOT_FOUND)
        return ConversationHandler.END


async def enter_amount(update: Update, _) -> int:
    await update.message.reply_text(SendMoneyReply.ENTER_AMOUNT, reply_markup=ReplyKeyboardRemove())
    return FINISH_SEND


async def finish_send(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    try:
        amount = Decimal(update.message.text)

        if amount <= 0:
            raise decimal.InvalidOperation

        acc_num_from = context.user_data.get("acc_from")
        acc_num_to = context.user_data.get("acc_to")

        await transfer_money(acc_num_from, acc_num_to, amount)
        await update.message.reply_text(SendMoneyReply.SEND_SUCCESS)

    except decimal.InvalidOperation:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.INCORRECT_AMOUNT)

    except IntegrityError:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.SEND_ERROR)

    except AccountNotFoundException:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.ACCOUNT_NOT_FOUND)

    except CantSendMoneyException:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=SendMoneyReply.INSUFFICIENT_FUNDS)

    return ConversationHandler.END


async def cancel(update: Update, _) -> int:
    await update.message.reply_text(SendMoneyReply.CANCEL)
    return ConversationHandler.END


send_money_handler = ConversationHandler(
    entry_points=[CommandHandler("send", start_send, ~filters.Text(["/cancel"]))],
    states={
        CHOOSE_SEND: [MessageHandler(filters.ALL & (~ filters.Text(["/cancel"])), choose_send_method)],
        PROCESS_SEND: [MessageHandler(filters.ALL & (~ filters.Text(["/cancel"])), process_send_method)],
        SEND_BY_USERNAME: [MessageHandler(filters.ALL & (~ filters.Text(["/cancel"])), process_send_by_username)],
        SEND_BY_ACCOUNT: [MessageHandler(filters.ALL & (~ filters.Text(["/cancel"])), process_send_by_account)],
        SEND_BY_CARD: [MessageHandler(filters.ALL & (~ filters.Text(["/cancel"])), process_send_by_card)],
        FINISH_SEND: [MessageHandler(filters.ALL & (~ filters.Text(["/cancel"])), finish_send)]
    },
    fallbacks=[CommandHandler("cancel", cancel)]
)
