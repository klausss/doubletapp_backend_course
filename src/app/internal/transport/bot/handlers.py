from django.core.exceptions import ValidationError
from telegram import Update
from telegram.ext import CallbackContext, ContextTypes

from app.internal.models.tguser import TgUser
from app.internal.services.banking_service import get_user_balance
from app.internal.services.custom_exceptions import UserNotFoundException, UserAlreadyExistsException, \
    CantSendMoneyException, FavoritesEmptyException, CardNotFoundException
from app.internal.services.user_service import get_user_by_id_async, save_user, set_user_phone_number, user_exists, \
    remove_user_from_favorites, list_favorite_users, add_user_to_favorites
from app.internal.transport.bot.replies import StartReply, SetPhoneReply, MeReply, BalanceReply, FavoritesReply, \
    CommonReply


async def start(update: Update, _):
    """
    Saves the information about user in DB: user's telegram id, username, first name and last name.
    """
    user_exists_f = await user_exists(update.effective_user.id)
    if user_exists_f:
        await update.message.reply_text(StartReply.USER_EXISTS)
        return

    user = TgUser(
        id=update.effective_chat.id,
        username=update.effective_chat.username,
        first_name=update.effective_chat.first_name,
        last_name=update.effective_chat.last_name,
    )
    await save_user(user)

    await update.message.reply_text(StartReply.USER_SAVED)


async def set_phone(update: Update, context: CallbackContext):
    """
    Requires a phone number (in Russian localization) as an argument from user.
    Checks if a number is valid and if so, saves it in the DB.
    """
    user_exists_f = await user_exists(update.effective_user.id)
    if not user_exists_f:
        await update.message.reply_text(SetPhoneReply.USER_DOESNT_EXIST)
        return

    if len(context.args) != 1:
        await update.message.reply_text(SetPhoneReply.INCORRECT_SET_PHONE)
        return

    phone_num = context.args[0]

    try:
        await set_user_phone_number(update.effective_user.id, phone_num)
    except ValidationError:
        await update.message.reply_text(SetPhoneReply.INCORRECT_SET_PHONE)
        return

    await update.message.reply_text(SetPhoneReply.PHONE_SET)


async def me(update: Update, _):
    """
    Sends to user info about them: id, username, phone number, first name and last name.
    """
    try:
        user = await get_user_by_id_async(update.effective_user.id)
        text = MeReply.USER_INFO.format(user_info=str(user))

        if not user.phone_number:
            text = MeReply.PHONE_ISNT_SET
    except UserNotFoundException:
        text = SetPhoneReply.USER_DOESNT_EXIST
    await update.message.reply_text(text)


async def check_balance(update: Update, _) -> None:
    try:
        balance = await get_user_balance(update.effective_user.id)
        text = BalanceReply.BALANCE
        for b in balance:
            text += f"{b[0]}: {b[1]} р."
    except CardNotFoundException:
        text = BalanceReply.SMTH_WRONG
    await update.message.reply_text(text)


async def add_user_to_favorites_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if len(context.args) < 1:
        await update.message.reply_text(FavoritesReply.INCORRECT_ADD)
        return
    username = context.args[0].replace("@", "")
    try:
        await add_user_to_favorites(update.effective_user.id, username)
        text = FavoritesReply.ADD_SUCCESS.format(username=username)
    except UserNotFoundException:
        text = FavoritesReply.USER_NOT_FOUND
    except UserAlreadyExistsException:
        text = FavoritesReply.USER_EXISTS_IN_FAV
    except CantSendMoneyException:
        text = FavoritesReply.CANT_SEND_MONEY
    await update.message.reply_text(text)


async def remove_user_from_favorites_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if len(context.args) < 1:
        await update.message.reply_text(FavoritesReply.INCORRECT_REMOVE)
        return
    username = context.args[0].replace("@", "")
    try:
        await remove_user_from_favorites(update.effective_user.id, username)
        text = FavoritesReply.REMOVE_SUCCESS.format(username=username)
    except UserNotFoundException:
        text = FavoritesReply.USER_NOT_FOUND
    await update.message.reply_text(text)


async def list_favorite_users_handler(update: Update, _) -> None:
    try:
        favorites = await list_favorite_users(update.effective_user.id)
        text = ""
        async for user in favorites:
            text += f"{user.username}, "
        text = text[:-2]
        text += FavoritesReply.FAVORITES
    except UserNotFoundException:
        text = FavoritesReply.USER_NOT_FOUND
    except FavoritesEmptyException:
        text = FavoritesReply.FAVORITES_EMPTY
    await update.message.reply_text(text)


async def help_command(update: Update, _) -> None:
    """
    Sends to user the list of available commands.
    """
    await update.message.reply_text(CommonReply.HELP)


async def unknown(update: Update, _):
    """
    Sends the offer to read 'help' to all unregistered commands.
    """
    await update.message.reply_text(CommonReply.UNKNOWN)
