from creditcards.models import CardExpiryField, CardNumberField, SecurityCodeField
from django.db import models

from app.internal.models.account import Account
from app.internal.models.tguser import TgUser


class Card(models.Model):
    card_number = CardNumberField(primary_key=True)
    expiry_date = CardExpiryField()
    cvv_code = SecurityCodeField()
    account_number = models.ForeignKey(Account, on_delete=models.PROTECT)
    card_owner = models.ForeignKey(TgUser, on_delete=models.PROTECT)
