from django.db import models

from app.internal.models.tguser import TgUser


class Account(models.Model):
    account_number = models.CharField(max_length=20, primary_key=True)
    owner = models.ForeignKey(TgUser, on_delete=models.PROTECT)
    money = models.DecimalField(max_digits=8, decimal_places=2, default=0)
