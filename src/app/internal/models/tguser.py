from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class TgUser(models.Model):
    """
    Model of a telegram user.
    """

    id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=32, unique=True)
    phone_number = PhoneNumberField(blank=True, null=True, unique=True, region="RU", default=None)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(blank=True, max_length=64)
    favorite_users = models.ManyToManyField("self", blank=True, symmetrical=False)

    def __str__(self):
        return (
            f"ID: {self.id},\nUsername: {self.username},\n"
            f"Номер телефона: {self.phone_number},\n"
            f"Имя: {self.first_name},\nФамилия: {self.last_name}"
        )
