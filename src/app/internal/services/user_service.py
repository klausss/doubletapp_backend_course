from phonenumber_field.validators import validate_international_phonenumber
from typing import Any
from app.internal.models.tguser import TgUser
from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.services.custom_exceptions import UserNotFoundException, UserAlreadyExistsException, \
    CantSendMoneyException, FavoritesEmptyException


async def save_user(user: TgUser) -> None:
    """
    Save telegram-user in the database.
    :param user: TgUser object
    :return:
    """
    await user.asave()


async def get_user_by_id_async(user_id: int) -> TgUser:
    """
    Get telegram user info from DB.
    :param user_id: user's telegram id
    :return: str (user_id, username, phone number, first_name, last_name)
    """
    user = await TgUser.objects.filter(id=user_id).afirst()
    if not user:
        raise UserNotFoundException
    return user


async def get_user_by_username(username: str) -> TgUser:
    user = await TgUser.objects.filter(username=username).afirst()
    if not user:
        raise UserNotFoundException
    return user


async def set_user_phone_number(user_id: int, phone_number: str) -> None:
    """
    Create or update user's phone number in DB.
    :param user_id: user's telegram id
    :param phone_number: user's phone number from the message
    :return: None
    :raises ValidationError
    """
    validate_international_phonenumber(phone_number)
    await TgUser.objects.filter(id=user_id).aupdate(phone_number=phone_number)


async def user_exists(user_id: int) -> bool:
    """
    Check if user exists in DB.
    :param user_id: user's telegram id
    :return: True - user exists in DB, False - doesn't.
    """
    return await TgUser.objects.filter(id=user_id).aexists()


def get_user_by_id(user_id: int) -> TgUser | None:
    """
    Get TgUser instance from DB by user's telegram id.
    :param user_id: user's telegram id
    :return: TgUser instance, if user is found. None, if user doesn't exist.
    """
    user = TgUser.objects.filter(id=user_id).first()
    if not user or not user.phone_number:
        return None
    return user


async def can_send_money_to_user(user: TgUser) -> bool:
    """
    Returns True if a user has a card and an account, False - otherwise.
    :param user: TgUser
    :return: bool
    """
    account = await Account.objects.filter(owner=user).aexists()
    card = await Card.objects.filter(card_owner=user).aexists()
    return account and card


async def add_user_to_favorites(user_id: int, username_to_add: str) -> None:
    """
    Add user to favorites list by the username
    :param user_id: int
    :param username_to_add: str
    :return: None
    :raises UserNotFoundException
    :raises CantSendMoneyException
    :raises UserAlreadyExistsException
    """
    user = await get_user_by_id_async(user_id)
    favorite_user = await get_user_by_username(username_to_add)
    can_send = await can_send_money_to_user(favorite_user)
    if not can_send or user.id == favorite_user.id:
        raise CantSendMoneyException

    user_already_exists = await user.favorite_users.filter(username=username_to_add).aexists()
    if user_already_exists:
        raise UserAlreadyExistsException
    await user.favorite_users.aadd(favorite_user)
    await user.asave()


async def remove_user_from_favorites(user_id: int, username_to_remove: str) -> None:
    """
    Removes a user from favorite list.
    :param user_id: int
    :param username_to_remove: str
    :return: None
    :raise UserNotFoundException
    """
    user = await get_user_by_id_async(user_id)
    user_to_remove = await get_user_by_username(username_to_remove)
    user_already_exists = await user.favorite_users.filter(username=username_to_remove).aexists()
    if not user_already_exists:
        raise UserNotFoundException
    await user.favorite_users.aremove(user_to_remove)
    await user.asave()


async def list_favorite_users(user_id: int) -> Any:
    """
    Returns the list of favorite users, if not empty.
    :param user_id: int
    :return: QuerySet[TgUser]
    :raises UserNotFoundException
    :raises FavoritesEmptyException
    """
    user = await get_user_by_id_async(user_id)
    # TODO мне нужны только юзернеймы
    favorites = user.favorite_users.values('username')
    not_empty = await favorites.aexists()
    if not not_empty:
        raise FavoritesEmptyException
    return favorites
