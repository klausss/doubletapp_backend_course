class NoPhoneNumberException(Exception):
    """
    Raised when a user hasn't filled the phone number.
    """

    pass


class UserNotFoundException(Exception):
    """
    Raised when a user isn't found is the database.
    """
    pass


class UserAlreadyExistsException(Exception):
    """
    Raised when a user already exists in a list of users (for example, in a favorite_users list).
    """
    pass


class CantSendMoneyException(Exception):
    """
    Raised when a user doesn't have an account and/or card.
    """
    pass


class CardNotFoundException(Exception):
    """
    Raised when the card number isn't found in db.
    """
    pass


class AccountNotFoundException(Exception):
    """
    Raised when the account number isn't found in db.
    """
    pass


class FavoritesEmptyException(Exception):
    """
    Raised when the list of favorite users is empty.
    """
    pass
