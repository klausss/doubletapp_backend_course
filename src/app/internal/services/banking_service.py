from typing import List, Tuple
from decimal import Decimal
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from asgiref.sync import sync_to_async
from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.services.user_service import get_user_by_username, get_user_by_id_async
from app.internal.services.custom_exceptions import CantSendMoneyException, CardNotFoundException, \
    AccountNotFoundException


async def get_user_balance(user_id: int) -> List[Tuple[str, str]]:
    """
    Returns the list of tuples of user's cards' last 4 digits and its balance.
    :param user_id: int
    :return: List[Tuple[4 digits, balance]]
    :raises UserNotFoundException
    :raises CardNotFoundException
    """
    user = await get_user_by_id_async(user_id)
    cards = Card.objects.filter(card_owner=user).select_related('account_number')
    cards_not_empty = await cards.aexists()
    if not cards_not_empty:
        raise CardNotFoundException
    balance = []
    async for card in cards:
        num = '*' + card.card_number[-4:]
        card_balance = card.account_number.money
        balance.append((num, card_balance))
    return balance


async def get_user_cards(user_id: int) -> List[Card]:
    """
    Returns a list of user's cards.
    :param user_id: int
    :return: List[Card]
    :raises UserNotFoundException
    """
    # TODO сделать фильтрацию сразу по user id
    user = await get_user_by_id_async(user_id)
    cards = [c async for c in Card.objects.filter(card_owner=user)]
    return cards


async def check_account_exists(account_number: str) -> bool:
    """
    Returns True if the specified account exists in the database, False - otherwise.
    :param account_number: str
    :return: bool
    """
    return await Account.objects.filter(account_number=account_number).aexists()


async def get_account_number_by_username(username: str) -> str | None:
    """
    Returns THE FIRST IN THE QUERY account number of the user, if they have any. Otherwise, exception is raised.
    :param username: recipient's username
    :return: account number (str) or None if the user or their account isn't found.
    :raises UserNotFoundException if a user with entered username isn't found in the database.
    :raises CantSendMoneyException if a user doesn't have accounts.
    """
    user = await get_user_by_username(username)
    accounts = [a async for a in Account.objects.filter(owner=user)]
    if not accounts:
        raise CantSendMoneyException
    return accounts[0].account_number


async def get_account_number_by_card(card_number: str) -> str:
    """
    Returns account number by card number
    :param card_number: str
    :return: account number, str
    :raises CardNotFoundException if the card isn't found in the database.
    """
    try:
        card = await Card.objects.select_related('account_number').aget(card_number=card_number)
    except ObjectDoesNotExist:
        raise CardNotFoundException
    return card.account_number.account_number


async def transfer_money(acc_num_from: str, acc_num_to: str, amount: Decimal) -> None:
    """
    Transfers money from the first account to the second.
    :param acc_num_from: str
    :param acc_num_to: str
    :param amount: Decimal
    :return: None
    :raises AccountNotFoundException if acc_from or acc_to isn't found in the database
    :raises CantSendMoneyException if the amount is bigger that the balance in the acc_from
    """
    acc_from = await Account.objects.filter(account_number=acc_num_from).afirst()
    acc_to = await Account.objects.filter(account_number=acc_num_to).afirst()

    if not acc_from or not acc_to:
        raise AccountNotFoundException

    if amount > acc_from.money:
        raise CantSendMoneyException
    await sync_to_async(transfer)(acc_from, acc_to, amount)


# TODO select user with account for update
@transaction.atomic
def transfer(acc_from: Account, acc_to: Account, amount: Decimal) -> None:
    acc_from.money -= amount
    acc_to.money += amount
    acc_from.save()
    acc_to.save()
