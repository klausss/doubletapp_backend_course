from django.contrib import admin

from app.internal.models.tguser import TgUser


@admin.register(TgUser)
class UserAdmin(admin.ModelAdmin):
    pass
