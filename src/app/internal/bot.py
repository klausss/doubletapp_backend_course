from telegram.ext import Application, CommandHandler, MessageHandler, filters

from app.internal.transport.bot.handlers import help_command, me, set_phone, start, unknown, \
    remove_user_from_favorites_handler, list_favorite_users_handler, check_balance, add_user_to_favorites_handler

from config.settings import BOT_TOKEN
from app.internal.transport.bot.send_money_conv_handler import send_money_handler


class Bot:
    """
    Telegram bot instance
    """

    def __init__(self):
        self.application = Application.builder().token(BOT_TOKEN).build()
        self.create_handlers()

    def run(self):
        """
        Run bot
        """
        # self.application.run_polling()
        self.application.run_webhook(
            listen='0.0.0.0',
            port=5000,
            webhook_url='https://ianikina.backend23.2tapp.cc/1',
            url_path='1'
        )

    def create_handlers(self):
        """
        Register bot handlers
        """
        self.application.add_handler(CommandHandler("start", start))
        self.application.add_handler(CommandHandler("me", me))
        self.application.add_handler(CommandHandler("set_phone", set_phone))
        self.application.add_handler(CommandHandler("balance", check_balance))
        self.application.add_handler(CommandHandler("add", add_user_to_favorites_handler))
        self.application.add_handler(CommandHandler("remove", remove_user_from_favorites_handler))
        self.application.add_handler(CommandHandler("list", list_favorite_users_handler))
        self.application.add_handler(CommandHandler("help", help_command))
        self.application.add_handler(send_money_handler)
        self.application.add_handler(MessageHandler(filters.ALL, unknown))


bot = Bot()
