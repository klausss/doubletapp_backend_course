from django.urls import path

from app.internal.transport.rest.handlers import TgUserInfo

urlpatterns = [path("me", TgUserInfo.as_view())]
