from django.core.management.base import BaseCommand

from app.internal.bot import bot


class Command(BaseCommand):
    def handle(self, *args, **options):
        bot.run()
