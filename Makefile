migrate:
	docker-compose run --rm app python3 src/manage.py migrate

makemigrations:
	docker-compose run --rm app python3 src/manage.py makemigrations
	docker-compose run app bash -c "sudo chown -R ${USER} src/app/migrations/"

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	python src/manage.py runserver localhost:8000

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	docker-compose run app bash -c "isort ."
	docker-compose run app bash -c "flake8 --config setup.cfg"
	docker-compose run app bash -c "black --config pyproject.toml ."

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

app:
	python src/manage.py runserver 0.0.0.0:8000

bot:
	python src/manage.py run_bot

build:
	docker-compose build

run_all:
	docker-compose up -d

stop:
	docker-compose down

reload:
	docker-compose down
	docker-compose build
	docker-compose up -d

test:
	docker-compose run --rm app pytest --pyargs src

retest:
	docker-compose down
	docker-compose build
	docker-compose run --rm app pytest --pyargs src